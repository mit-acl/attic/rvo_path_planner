#ifndef _RVOPPAGENT_H
#define _RVOPPAGENT_H

#include <ros/ros.h>
#include <vector>
#include <deque>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/TwistStamped.h>
#include <acl_msgs/Waypoint.h>
#include <acl_msgs/QuadGoal.h>
#include <rvo_path_planner/RvoTraj.h>

#include "AgentTraj.h"
#include <acl/utils.hpp>

class RVOPPAgent{
public:
  struct Para{
    // === RVO core parameters ===
    float radius;
    float maxSpeed;
    float maxPlanAcc;
    float maxCommandAcc;
    int maxNeighbors;
    float neighborDist;
    float timeHorizon;
    float timeHorizonObst;
    // === RVO path planner parameters ===
    float previewTimeAcc;
    float previewTime;
    float goalTolerance_v;
    float goalTolerance_h;
    bool useLastGoal;
  };

  // Methods
  RVOPPAgent(ros::NodeHandle* nh, std::string veh_name, Para para_in);
  ~RVOPPAgent();

  // Members
  std::string name;
  
  geometry_msgs::PoseStamped pos_msg;
  geometry_msgs::TwistStamped vel_msg;

  ros::Time waypoints_issue_time;
  // geometry_msgs::Point waypoint;
  std::deque<geometry_msgs::Point> waypoint_queue;
  float desired_yaw;

  
  acl_msgs::QuadGoal lastGoal;

  ros::Time plan_start_time;
  RVO::AgentTraj traj;

  Para para;

  // Status
  bool reachedGoal_h;
  bool reachedGoal_v;
  bool isActivated;
  bool holdPosition;
  bool motorOn;

  // === Publisher and Subscribers ===
  ros::Subscriber sub_pos;
  ros::Subscriber sub_vel;
  ros::Subscriber sub_waypoint;
  ros::Publisher pub_quadGoal;
  ros::Publisher pub_rvoTraj;

  void cb_pos(const geometry_msgs::PoseStamped::ConstPtr& veh_pose_msg);
  void cb_vel(const geometry_msgs::TwistStamped::ConstPtr& veh_vel_msg);
  void cb_waypoint(const acl_msgs::Waypoint::ConstPtr& waypoint_msg);
  void publish_quadGoal();
  void publish_rvoTraj();

  // === Set and Get === //
  void setPos(geometry_msgs::PoseStamped pos);
  void setVel(geometry_msgs::TwistStamped vel);
  void setTraj(RVO::AgentTraj traj, ros::Time start_time);
  void setWaypoints(geometry_msgs::Point waypoint, ros::Time issue_time);
  void setWaypoints(std::vector<geometry_msgs::Point> waypoints, ros::Time issue_time);

  bool hasReachedGoal();
  rvo_path_planner::RvoTraj getRvoTrajMsg();
  float getMaxSpeedForPlanner();
  RVO::Vector2 getPosVec2();
  RVO::Vector2 getVelVec2();
  RVO::Vector2 getWaypointVec2();
  geometry_msgs::Point getCurrentWayPoint();
  acl_msgs::QuadGoal getControlGoal(float time_after_plan);
  RVO::Vector2 getLastGoalPosVec2();
  RVO::Vector2 getLastGoalVelVec2();
  float getCurrentYaw();  

  // === Methods === //
  void resetGoal();
  void resetWaypoint();
  void updateStatus();

};

#endif /* _RVOPPAGENT_H */
