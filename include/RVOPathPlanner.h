#ifndef _RVO_PATHPLANNER_H
#define _RVO_PATHPLANNER_H

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

// #define _OPENMP

#ifdef _OPENMP
#include <omp.h>
#endif

#include "RVO.h"
#include "AgentTraj.h"
#include "RVOPPAgent.h"

#ifndef M_PI
const float M_PI = 3.14159265358979323846f;
#endif

namespace RVO{

class RVOPathPlanner{ 
public:
  RVOPathPlanner();
  ~RVOPathPlanner();
  
  std::vector<RVOPPAgent*> agents;
  std::vector< std::vector<Vector2> > obstacles;

  // Simulation parameters
  float goalThreshold;
  float simTimeStep;
  int maxSteps;

  void setSimParameters(float goalThreshold, float simTimeStep, int maxSteps);
  void addAgent(RVOPPAgent* agentPtr);

  int addObstacle(std::vector<Vector2> obsVec);
  bool genPath();
  void initSim();

private:
  RVO::RVOSimulator sim;
  Vector2 getPrefVelByGoal(Vector2 currentPos, Vector2 goalPos);
  Vector2 getRateLimitedPrefVel(Vector2 currentVel, Vector2 prefVel, float maxAcc);
  bool reachedGoal();
  void setPreferredVelocities();
  void logTrajData();
  void simStep();
};
}

#endif /* _RVO_PATHPLANNER_H */