#include "RVOPathPlanner.h"

namespace RVO{

  RVOPathPlanner::RVOPathPlanner()
  {
    simTimeStep = 0.05f;
    goalThreshold = 0.05f;
    maxSteps = 200;    
  }
  RVOPathPlanner::~RVOPathPlanner()
  {
    // TODO: proper cleanup
  }
  
  void RVOPathPlanner::addAgent(RVOPPAgent* agentPtr)
  {
    if (agentPtr){
      agents.push_back(agentPtr);
    }
  }

  int RVOPathPlanner::addObstacle(std::vector<Vector2> obsVec)
  {
    obstacles.push_back(obsVec);
    return obstacles.size() - 1;
  }

  bool RVOPathPlanner::reachedGoal()
  {
    /* Check if all agents have reached their goals. */
    for (size_t i = 0; i < sim.getNumAgents(); ++i) {
      if (RVO::absSq(sim.getAgentPosition(i) - agents[i]->getWaypointVec2()) > goalThreshold*goalThreshold) {
        return false;
      }
    }
    return true;
  }

  Vector2 RVOPathPlanner::getRateLimitedPrefVel(Vector2 currentVel, Vector2 prefVel, float maxAcc)
  {
    Vector2 diff_vec = prefVel - currentVel;
    if (abs(diff_vec) <= simTimeStep*maxAcc){
      return prefVel;
    }
    else{
      return currentVel + simTimeStep*maxAcc*normalize(diff_vec);
    }
  }

  void RVOPathPlanner::setPreferredVelocities()
  {
    /*
    * Set the preferred velocity to be a vector of unit magnitude (speed) in the
    * direction of the goal.
    */
    #ifdef _OPENMP
    #pragma omp parallel for
    #endif
    for (int i = 0; i < static_cast<int>(sim.getNumAgents()); ++i) {
      Vector2 prefVel;
      prefVel = getPrefVelByGoal(sim.getAgentPosition(i),agents[i]->getWaypointVec2());
      sim.setAgentPrefVelocity(i, sim.getAgentMaxSpeed(i)*prefVel);
      // === Do rate limit ===
      Vector2 currentVel = sim.getAgentVelocity(i);
      prefVel = sim.getAgentPrefVelocity(i);
      sim.setAgentPrefVelocity(i,getRateLimitedPrefVel(currentVel,prefVel,agents[i]->para.maxPlanAcc));
      // === Perturb a little to avoid deadlocks due to perfect symmetry ===
      float angle = std::rand() * 2.0f * M_PI / RAND_MAX;
      float dist = std::rand() * 0.0001f / RAND_MAX;
      sim.setAgentPrefVelocity(i, sim.getAgentPrefVelocity(i) +
                                dist * Vector2(std::cos(angle), std::sin(angle)));
    }

  }

  void RVOPathPlanner::logTrajData()
  {
    for (int i = 0; i < sim.getNumAgents(); i++){
      agents[i]->traj.log_data(sim.getGlobalTime(),
        sim.getAgentPosition(i),
        sim.getAgentVelocity(i),
        sim.getAgentPrefVelocity(i));
    }
  }

  void RVOPathPlanner::simStep()
  {
    setPreferredVelocities();
    logTrajData();
    if (sim.getGlobalTime() > 0.25){
      simTimeStep = 0.1;
      sim.setTimeStep(0.1);
    }
    sim.doStep();
  }

  bool RVOPathPlanner::genPath(){
    // resetSim();
    initSim();
    bool goalReached = false;
    for (int step = 0; step < maxSteps; step++){
      simStep();
      if (reachedGoal()){
        goalReached = true;
        break;
      }
    }
    return goalReached;
  }

  void RVOPathPlanner::setSimParameters(float goalThreshold, float simTimeStep, int maxSteps)
  {
    this->goalThreshold = goalThreshold;
    this->simTimeStep = simTimeStep;
    this->maxSteps = maxSteps;
  }

  void RVOPathPlanner::initSim()
  {    
    // Setup parameters
    sim.setTimeStep(simTimeStep);

    // Add agents
    for (int i = 0; i < agents.size();i++){
      RVOPPAgent::Para agentPara = agents[i]->para;
      if (i == 0){
        // Use the first agent to set the Defaults. Will be overwritten by each individual agent later.
        sim.setAgentDefaults(
          agentPara.neighborDist,
          agentPara.maxNeighbors,
          agentPara.timeHorizon,
          agentPara.timeHorizonObst,
          agentPara.radius,
          agentPara.maxSpeed);
      }

      agents[i]->traj.set_agent_id(i); //TODO is this still necessary?
      agents[i]->traj.reset();

      // TODO also check the difference betwee lastGoal and current status.
      if (agents[i]->para.useLastGoal){
        sim.addAgent(agents[i]->getLastGoalPosVec2()); //Set according to useLastGoal
        sim.setAgentVelocity(i,agents[i]->getLastGoalVelVec2());
      }
      else{
        sim.addAgent(agents[i]->getPosVec2()); //Set according to useLastGoal
        sim.setAgentVelocity(i,agents[i]->getVelVec2());        
      }
      sim.setAgentNeighborDist(i,agentPara.neighborDist);
      sim.setAgentMaxNeighbors(i,agentPara.maxNeighbors);
      sim.setAgentTimeHorizon(i,agentPara.timeHorizon);
      sim.setAgentTimeHorizonObst(i,agentPara.timeHorizonObst);
      sim.setAgentRadius(i,agentPara.radius);
      sim.setAgentMaxSpeed(i,agents[i]->getMaxSpeedForPlanner());
    }
    // Add obstacles
    for (int i = 0; i < obstacles.size();i++){
      sim.addObstacle(obstacles[i]);
    }
    sim.processObstacles();
  }

  Vector2 RVOPathPlanner::getPrefVelByGoal(Vector2 currentPos, Vector2 goalPos)
  {
    // Out of ref trajectory, just use the goal
    Vector2 goalVector = goalPos - currentPos;
    if (absSq(goalVector) > 1.0f) {
      goalVector = normalize(goalVector);
    }
    return goalVector;
  }
}