#include "RVOPPAgent.h"
RVOPPAgent::RVOPPAgent(ros::NodeHandle* nh, std::string veh_name, Para para_in)
{
  name = veh_name;
  para = para_in;

  reachedGoal_h = true;
  reachedGoal_v = true;
  isActivated = false;
  holdPosition = true;
  motorOn = true;

  desired_yaw = 0.0;

  plan_start_time = ros::Time::now();
  
  // Setup subscriber and publisher 
  sub_pos = nh->subscribe(name+"/pose", 1, &RVOPPAgent::cb_pos, this);
  sub_vel = nh->subscribe(name+"/vel", 1, &RVOPPAgent::cb_vel, this);
  sub_waypoint = nh->subscribe(name+"/quad_waypoint", 1, &RVOPPAgent::cb_waypoint, this);
  
  pub_quadGoal = nh->advertise<acl_msgs::QuadGoal>(name+"/goal", 1);
  pub_rvoTraj = nh->advertise<rvo_path_planner::RvoTraj>(name+"/rvo_traj", 1);

  resetGoal();
  resetWaypoint();
}
RVOPPAgent::~RVOPPAgent()
{
  // TODO: Proper destructor
  // unsubscribe and all
}

void RVOPPAgent::resetGoal()
{
  // Set lastGoal to the current actual postiion
  lastGoal.header.stamp = pos_msg.header.stamp;
  lastGoal.pos.x = pos_msg.pose.position.x;
  lastGoal.pos.y = pos_msg.pose.position.y;
  lastGoal.pos.z = pos_msg.pose.position.z;
}
void RVOPPAgent::resetWaypoint()
{
  setWaypoints(pos_msg.pose.position, pos_msg.header.stamp);
  desired_yaw = acl::quat2yaw(pos_msg.pose.orientation.w,pos_msg.pose.orientation.x,pos_msg.pose.orientation.y,pos_msg.pose.orientation.z);
}

void RVOPPAgent::setPos(geometry_msgs::PoseStamped veh_pose_msg)
{
  pos_msg = veh_pose_msg;
}
void RVOPPAgent::setVel(geometry_msgs::TwistStamped vel_msg_in)
{
  vel_msg = vel_msg_in;
}
void RVOPPAgent::setTraj(RVO::AgentTraj traj, ros::Time start_time)
{
  this->traj = traj;
  this->plan_start_time = start_time;
}

// === Callbacks ===
void RVOPPAgent::cb_pos(const geometry_msgs::PoseStamped::ConstPtr& veh_pose_msg)
{
  // ROS_INFO_STREAM("=====" << name << "cb_pos() =====");
  setPos(*veh_pose_msg);
  if (not isActivated){
    resetWaypoint();
    resetGoal();
  }
  updateStatus();
}

void RVOPPAgent::cb_vel(const geometry_msgs::TwistStamped::ConstPtr& veh_vel_msg)
{
  setVel(*veh_vel_msg);
}

void RVOPPAgent::cb_waypoint(const acl_msgs::Waypoint::ConstPtr& waypoint_msg)
{
  if (not isActivated){
    ROS_INFO_STREAM(name << " not actiaved. No commands sent to " << name);
  }
  else{
    holdPosition = false;
    setWaypoints(waypoint_msg->goal_pose.position,waypoint_msg->header.stamp);
    double yaw = acl::quat2yaw(waypoint_msg->goal_pose.orientation.w,waypoint_msg->goal_pose.orientation.x,waypoint_msg->goal_pose.orientation.y,waypoint_msg->goal_pose.orientation.z);
    desired_yaw = yaw;
    ROS_INFO_STREAM("Set " << name << " waypoint to"
      << " x: " << waypoint_msg->goal_pose.position.x
      << " y: " << waypoint_msg->goal_pose.position.y
      << " z: " << waypoint_msg->goal_pose.position.z );
  }
}

// === Publish === //
void RVOPPAgent::publish_quadGoal()
{
  if (isActivated){
    double time_after_plan = (ros::Time::now() - plan_start_time).toSec();
    acl_msgs::QuadGoal goal = getControlGoal(time_after_plan);
    pub_quadGoal.publish(goal);
    lastGoal = goal;
  }
}

void RVOPPAgent::publish_rvoTraj()
{
  pub_rvoTraj.publish(getRvoTrajMsg());
}



RVO::Vector2 RVOPPAgent::getPosVec2()
{
  return RVO::Vector2(pos_msg.pose.position.x,pos_msg.pose.position.y);
}

RVO::Vector2 RVOPPAgent::getVelVec2()
{
  return RVO::Vector2(vel_msg.twist.linear.x,vel_msg.twist.linear.y);
}

RVO::Vector2 RVOPPAgent::getWaypointVec2()
{
  // return RVO::Vector2(waypoint_queue.front().position.x,waypoint_queue.front().position.y);
  geometry_msgs::Point waypoint = getCurrentWayPoint();
  return RVO::Vector2(waypoint.x,waypoint.y);
}

RVO::Vector2 RVOPPAgent::getLastGoalPosVec2()
{
  return RVO::Vector2(lastGoal.pos.x,lastGoal.pos.y);
}

RVO::Vector2 RVOPPAgent::getLastGoalVelVec2()
{
  return RVO::Vector2(lastGoal.vel.x,lastGoal.vel.y);
}

float RVOPPAgent::getCurrentYaw()
{
  return acl::quat2yaw(pos_msg.pose.orientation.w,pos_msg.pose.orientation.x,pos_msg.pose.orientation.y,pos_msg.pose.orientation.z);
}

rvo_path_planner::RvoTraj RVOPPAgent::getRvoTrajMsg()
{
  // Construct the RvoTrajMsg of given vehicle
  rvo_path_planner::RvoTraj msg;
  
  // Header
  msg.header.stamp = plan_start_time; //Use the time when the path was planned.
  
  // current posiiton
  msg.pos_current = pos_msg.pose.position;

  // position trajectory and time trajectory
  for (int i = 0; i < traj.pos_traj.size(); i++){
    geometry_msgs::Point p;
    p.x = (float) traj.pos_traj[i].x();
    p.y = (float) traj.pos_traj[i].y();
    p.z = (float) 0.0;
    msg.pos_traj.push_back(p);
    msg.time_traj.push_back(traj.time_traj[i]);
  }
  // Radius
  msg.radius = para.radius; //TODO use agent specific radius
  // Goal
  msg.goal = getCurrentWayPoint();
  return msg;
}

float RVOPPAgent::getMaxSpeedForPlanner()
{
  if (holdPosition){
    return 0.0f;
  }
  return para.maxSpeed;
}

bool RVOPPAgent::hasReachedGoal()
{
  if (waypoint_queue.size() <= 1){
    return reachedGoal_h && reachedGoal_v;
  }
  else{
    return false;
  }
}

void RVOPPAgent::updateStatus()
{
  // TODO: Update reach goal mechanism
  RVO::Vector2 currentWaypoint = getWaypointVec2();
  float dist_to_goal_h = RVO::abs(getPosVec2() - getWaypointVec2());
  float dist_to_goal_v = std::abs(pos_msg.pose.position.z - getCurrentWayPoint().z);
  if (dist_to_goal_v < para.goalTolerance_v){
    reachedGoal_v = true;
  }
  if (dist_to_goal_h < para.goalTolerance_h && RVO::abs(getVelVec2()) < 0.1){
    reachedGoal_h = true;
  }

  if (reachedGoal_h && reachedGoal_v && waypoint_queue.size() > 1){
    waypoint_queue.pop_front();
    reachedGoal_h = false;
    reachedGoal_v = false;
  }
  // ROS_INFO_STREAM("==== updateStatus end ====");
}

geometry_msgs::Point RVOPPAgent::getCurrentWayPoint(){
  // // TODO: make sure not empty.
  if (waypoint_queue.size() < 1){
    resetWaypoint();
  }
  return waypoint_queue.front();
  // return this->waypoint;
}

void RVOPPAgent::setWaypoints(std::vector<geometry_msgs::Point> waypoints_vec, ros::Time issue_time)
{
  // ROS_INFO_STREAM("======== setWaypoints ========");
  // ROS_INFO_STREAM("size before clear():" << waypoint_queue.size());
  waypoint_queue.clear();
  // ROS_INFO_STREAM("size after clear():" << waypoint_queue.size());
  for (int i = 0; i < waypoints_vec.size(); i++){
    waypoint_queue.push_back(waypoints_vec[i]);
  }
  // ROS_INFO_STREAM("size after push_backs:" << waypoint_queue.size());
  reachedGoal_h = false;
  reachedGoal_v = false;
  waypoints_issue_time = issue_time;
}

void RVOPPAgent::setWaypoints(geometry_msgs::Point waypoint, ros::Time issue_time)
{
  std::vector<geometry_msgs::Point> waypoints_vec;
  waypoints_vec.push_back(waypoint);
  setWaypoints(waypoints_vec,issue_time);

  // this->waypoint = waypoint;
  // waypoints_issue_time = issue_time;
  // reachedGoal_v = false;
  // reachedGoal_h = false;
}


acl_msgs::QuadGoal RVOPPAgent::getControlGoal(float time_after_plan)
{
  RVO::Vector2 goal_pos;
  RVO::Vector2 goal_vel;
  float goal_time = time_after_plan + para.previewTime;
  traj.get_state_at_time(goal_time,goal_pos,goal_vel);

  acl_msgs::QuadGoal goal;
  goal.waypointType = 1;

  // === Compute x and y === //
  goal.pos.x = goal_pos.x();
  goal.pos.y = goal_pos.y();
  goal.vel.x = goal_vel.x();
  goal.vel.y = goal_vel.y();

  // === Compute rate limited z === //
  float waypoint_z = getCurrentWayPoint().z;
  float lastGoal_z = lastGoal.pos.z;
  float goal_z;
  if (waypoint_z > lastGoal_z){
    goal_z = std::min(waypoint_z, lastGoal_z + 0.004f);
  }
  else{
    goal_z = std::max(waypoint_z, lastGoal_z - 0.0020f);
  }
  goal.pos.z = goal_z;  
  goal.vel.z = 0.0;

  // Compute desired acclearation
  RVO::Vector2 current_vel = getVelVec2();
  float goal_time_acc = time_after_plan + para.previewTimeAcc;
  traj.get_state_at_time(goal_time_acc,goal_pos,goal_vel);
  RVO::Vector2 goal_acc = (goal_vel - current_vel)/goal_time_acc;
  if (RVO::abs(goal_acc) > para.maxCommandAcc){
    goal_acc = RVO::normalize(goal_acc)*para.maxCommandAcc;
  }
  goal.accel.x = goal_acc.x();
  goal.accel.y = goal_acc.y();

  // For landing
  if (goal.pos.z <= -1.0){
    motorOn = false;
    reachedGoal_v = true;
  }

  // Check for kill motor
  if (not motorOn){
    goal.waypointType = 2;
  }

  goal.yaw = desired_yaw;
  goal.header.stamp = ros::Time::now();
  // lastGoal = goal;
  // lastGoalInitialized = true;
  return goal;
}
