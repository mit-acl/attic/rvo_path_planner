#include <ros/ros.h>
#include <vector>
#include <map>

#include "RVOPathPlanner.h"
#include "RVOPPAgent.h"
#include "AgentTraj.h"

// === Custom msgs ===
#include <rvo_path_planner/CommandSequence.h>
#include <rvo_path_planner/CommandComplete.h>
#include <rvo_path_planner/RvoTraj.h>
#include <rvo_path_planner/RvoRoom.h>

// === Standard msgs ===
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/ColorRGBA.h>
#include <std_srvs/Empty.h>

// acl_msgs 
#include <acl_msgs/VehicleList.h> //TODO Add dependency 
#include <acl_msgs/QuadGoal.h>
#include <acl_msgs/Waypoint.h>

#include <acl/utils.hpp>

class RVOPPNode{
  typedef std::map<std::string,RVOPPAgent*>::iterator VehIter;

public:
  ros::NodeHandle nh;
  ros::NodeHandle nh_p;

  ros::Subscriber sub_veh_list;
  ros::Subscriber sub_command_seq;
  ros::Publisher pub_traj_marker_array;
  ros::Publisher pub_command_complete;
  ros::Publisher pub_room;

  std::map<std::string,RVOPPAgent*> veh_map;
  std::map<std::string,RVOPPAgent::Para> veh_para_type_map;
  
  std::vector<ros::ServiceServer> srv_list;
  std::vector<std_msgs::ColorRGBA> color_list;

  std::vector<std::string> veh_type_list;

  // === Parameters === //  
  double goalThreshold;
  double simTimeStep;
  int maxSteps;
  // int numIterate;
  double defaultTakoffHeight;
  double defaultLandHeight;
  double defaultLowHeight;
  int room_type;

  std::string node_name_str;

  // === Room and obstacle === //
  std::vector< std::vector<RVO::Vector2> > obstacles;

  // === Methods === //
  RVOPPNode()
  {
    nh = ros::NodeHandle();
    nh_p = ros::NodeHandle("~");
    node_name_str = "[" + ros::this_node::getName() + "] ";

    // isActivated = false;

    readParameters();
    populate_color_list();
    populate_para_map();

    sub_veh_list = nh.subscribe("current_vehicles",1, &RVOPPNode::callback_veh_list, this);
    sub_command_seq = nh_p.subscribe("command",10, &RVOPPNode::callback_command, this);
    pub_traj_marker_array = nh.advertise<visualization_msgs::MarkerArray>("/RAVEN_vehicles", 1);
    pub_command_complete = nh_p.advertise<rvo_path_planner::CommandComplete>("command_complete",10);
    pub_room = nh_p.advertise<rvo_path_planner::RvoRoom>("rvo_room",1,true);
    genObstacles();

    srv_list.push_back(nh_p.advertiseService("activate", &RVOPPNode::callback_srv_activate ,this));
    srv_list.push_back(nh_p.advertiseService("deactivate", &RVOPPNode::callback_srv_deactivate ,this));
    srv_list.push_back(nh_p.advertiseService("land_all", &RVOPPNode::callback_srv_land_all ,this));
    srv_list.push_back(nh_p.advertiseService("takeoff_all", &RVOPPNode::callback_srv_takeoff_all ,this));
    srv_list.push_back(nh_p.advertiseService("kill_all", &RVOPPNode::callback_srv_kill_all ,this));

    ROS_INFO_STREAM(node_name_str << "Call /activate or /takeoff_all to activate the path planner.");

  }
  ~RVOPPNode(){}


  RVOPPAgent::Para getTypePara(std::string veh_type)
  {
    RVOPPAgent::Para para;
    double radius; ros::param::getCached("~" + veh_type +"/radius",radius);
    double maxSpeed; ros::param::getCached("~" + veh_type +"/maxSpeed",maxSpeed);
    double maxPlanAcc; ros::param::getCached("~" + veh_type +"/maxPlanAcc",maxPlanAcc);
    double maxCommandAcc; ros::param::getCached("~" + veh_type +"/maxCommandAcc",maxCommandAcc);
    int maxNeighbors; ros::param::getCached("~" + veh_type +"/maxNeighbors",maxNeighbors);
    double neighborDist; ros::param::getCached("~" + veh_type +"/neighborDist",neighborDist);
    double timeHorizon; ros::param::getCached("~" + veh_type +"/timeHorizon",timeHorizon);
    double timeHorizonObst; ros::param::getCached("~" + veh_type +"/timeHorizonObst",timeHorizonObst);
    double previewTimeAcc; ros::param::getCached("~" + veh_type +"/previewTimeAcc",previewTimeAcc);
    double previewTime; ros::param::getCached("~" + veh_type +"/previewTime",previewTime);
    double goalTolerance_v; ros::param::getCached("~" + veh_type +"/goalTolerance_v",goalTolerance_v);
    double goalTolerance_h; ros::param::getCached("~" + veh_type +"/goalTolerance_h",goalTolerance_h);
    bool useLastGoal; ros::param::getCached("~" + veh_type +"/useLastGoal",useLastGoal);
    
    para.radius = radius;
    para.maxSpeed = maxSpeed;
    para.maxPlanAcc = maxPlanAcc;
    para.maxCommandAcc = maxCommandAcc;
    para.maxNeighbors = maxNeighbors;
    para.neighborDist = neighborDist;
    para.timeHorizon = timeHorizon;
    para.timeHorizonObst = timeHorizonObst;
    para.previewTimeAcc = previewTimeAcc;
    para.previewTime = previewTime;
    para.goalTolerance_v = goalTolerance_v;
    para.goalTolerance_h = goalTolerance_h;
    para.useLastGoal = useLastGoal;
    return para;
  }

  void populate_para_map()
  {
    for (int i = 0; i < veh_type_list.size(); i++){
      std::string veh_type = veh_type_list[i];
      veh_para_type_map[veh_type]=getTypePara(veh_type);
    }
  }

  void populate_color_list()
  {
    std_msgs::ColorRGBA color;
    color.r = 1.0;
    color.g = 0.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 1.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 0.0;
    color.b = 1.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 1.0;
    color.g = 0.0;
    color.b = 1.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 1.0;
    color.g = 1.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.5;
    color.g = 0.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 0.5;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 0.0;
    color.b = 0.5;
    color.a = 1.0;
    color_list.push_back(color);
  }

  void readParameters()
  {
    // Read from ROS parameters
    ros::param::getCached("~goalThreshold",goalThreshold);
    ros::param::getCached("~simTimeStep",simTimeStep);
    ros::param::getCached("~maxSteps",maxSteps);
    ros::param::getCached("~defaultTakoffHeight",defaultTakoffHeight);
    ros::param::getCached("~defaultLandHeight",defaultLandHeight);
    ros::param::getCached("~defaultLowHeight",defaultLowHeight);
    ros::param::getCached("~room_type",room_type);
    ros::param::getCached("~veh_type_list",veh_type_list);
  }


  void genObstacles()
  {
    rvo_path_planner::RvoRoom roomMsg;

    // Pillar
    if (room_type == 0){
      std::vector<RVO::Vector2> obs;
            // ===== The pillar ===== //
      obs.push_back(RVO::Vector2(-2.6f,-3.7f));
      obs.push_back(RVO::Vector2(-3.9f,-3.7f));
      obs.push_back(RVO::Vector2(-4.2f,-4.35f));
      obs.push_back(RVO::Vector2(-3.9f,-5.0f));
      obs.push_back(RVO::Vector2(-2.6f,-5.0f));
      obs.push_back(RVO::Vector2(-2.2f,-4.35f));

      obstacles.push_back(obs);
      for (int i = 0; i < obs.size(); i++){
        geometry_msgs::Point p;
        p.x = obs[i].x();
        p.y = obs[i].y();
        p.z = 0.0f;
        roomMsg.pillar.push_back(p);
      }
    }

    // The room
    std::vector<RVO::Vector2> obs;
    if (room_type == 0){
      // ===== Full L-shape space  ===== //
      obs.push_back(RVO::Vector2(2.0f, 2.5f));
      obs.push_back(RVO::Vector2(2.0f, -6.9f));
      obs.push_back(RVO::Vector2(-7.5f, -6.9f));
      obs.push_back(RVO::Vector2(-7.5f, -1.8f));
      obs.push_back(RVO::Vector2(-2.0f, -1.8f));
      obs.push_back(RVO::Vector2(-2.0f, 2.5f));
      obstacles.push_back(obs);

      for (int i = 0; i < obs.size(); i++){
        geometry_msgs::Point p;
        p.x = obs[i].x();
        p.y = obs[i].y();
        p.z = 0.0f;
        roomMsg.room.push_back(p);
      }

    }
    else if (room_type == 1){
      // ===== Only the main space ===== // 
      obs.push_back(RVO::Vector2(2.0f, 2.5f));
      obs.push_back(RVO::Vector2(2.0f, -6.9f));
      obs.push_back(RVO::Vector2(-2.0f, -6.9f));
      obs.push_back(RVO::Vector2(-2.0f, 2.5f));
      obstacles.push_back(obs);

      for (int i = 0; i < obs.size(); i++){
        geometry_msgs::Point p;
        p.x = obs[i].x();
        p.y = obs[i].y();
        p.z = 0.0f;
        roomMsg.room.push_back(p);
      }
    }
    else if (room_type == 2){
      // ===== Relaxed main space ===== // 
      obs.push_back(RVO::Vector2(2.2f, 3.0f));
      obs.push_back(RVO::Vector2(2.2f, -7.3f));
      obs.push_back(RVO::Vector2(-2.2f, -7.3f));
      obs.push_back(RVO::Vector2(-2.2f, 3.0f));
      obstacles.push_back(obs);

      for (int i = 0; i < obs.size(); i++){
        geometry_msgs::Point p;
        p.x = obs[i].x();
        p.y = obs[i].y();
        p.z = 0.0f;
        roomMsg.room.push_back(p);
      }
    }


    pub_room.publish(roomMsg);
  }

  void addVehicle(std::string veh_name)
  {  
    std::map<std::string,RVOPPAgent::Para>::iterator veh_type_iter = veh_para_type_map.find(veh_name.substr(0,2));
    if (veh_type_iter != veh_para_type_map.end()){
      // veh type is handled
      RVOPPAgent::Para paraStruct = veh_type_iter->second;
      // Create RVOPPAgent using name and parameters
      RVOPPAgent* veh = new RVOPPAgent(&nh, veh_name, paraStruct);
      // Add to veh_map
      veh_map[veh_name] = veh;
      ROS_INFO_STREAM("[path_planner] " << veh_name << "added.");
    }
  }

  // void removeVehilce(std::string veh_name)
  // {
  //   // TODO: Implement remove vehicle.
  // }

  void publishRvoTrajMsgs()
  {
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      vehIter->second->publish_rvoTraj();
    }
  }

  visualization_msgs::MarkerArray getTrajMarkerArray()
  {
    visualization_msgs::MarkerArray markerArray;
    int tempIndex = 0;
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      RVOPPAgent* veh = vehIter->second;

      // === Trajectory === //
      RVO::AgentTraj agentTraj = veh->traj;

      visualization_msgs::Marker traj_marker;
      traj_marker.header.frame_id = "/world";
      traj_marker.header.stamp = ros::Time::now();
      traj_marker.ns = "rvo_path";
      traj_marker.id = tempIndex;
      traj_marker.action = visualization_msgs::Marker::ADD;
      traj_marker.lifetime = ros::Duration(1.0);
      traj_marker.type = visualization_msgs::Marker::LINE_STRIP;
      traj_marker.pose.orientation.w = 1.0;
      traj_marker.color = color_list[tempIndex];
      traj_marker.scale.x = 0.045;
      traj_marker.scale.y = 0.03;
      traj_marker.scale.z = 0.03;
      // Fill in the traj points
      for (int j = 0; j < agentTraj.pos_traj.size();j++){
        geometry_msgs::Point p;
        p.x = (float) agentTraj.pos_traj[j].x();
        p.y = (float) agentTraj.pos_traj[j].y();
        p.z = (float) 0.0;
        traj_marker.points.push_back(p);
      }
      markerArray.markers.push_back(traj_marker);

      // === Goal === //
      visualization_msgs::Marker goal_marker;
      goal_marker.header.frame_id = "/world";
      goal_marker.header.stamp = ros::Time::now();
      goal_marker.ns = "rvo_goal";
      goal_marker.id = tempIndex;
      goal_marker.action = visualization_msgs::Marker::ADD;
      goal_marker.lifetime = ros::Duration(1.0);
      goal_marker.type = visualization_msgs::Marker::CYLINDER;
      goal_marker.pose.position.x = veh->getCurrentWayPoint().x;
      goal_marker.pose.position.y = veh->getCurrentWayPoint().y;
      goal_marker.pose.position.z = veh->getCurrentWayPoint().z/2.0;
      goal_marker.pose.orientation.w = 1.0;
      goal_marker.color = color_list[tempIndex];
      goal_marker.color.a = 0.25;
      goal_marker.scale.x = 0.1;
      goal_marker.scale.y = 0.1;
      goal_marker.scale.z = veh->getCurrentWayPoint().z;
      markerArray.markers.push_back(goal_marker);

      // === Pos === //
      visualization_msgs::Marker pos_marker;
      pos_marker.header.frame_id = "/world";
      pos_marker.header.stamp = ros::Time::now();
      pos_marker.ns = "rvo_pos";
      pos_marker.id = tempIndex;
      pos_marker.action = visualization_msgs::Marker::ADD;
      pos_marker.lifetime = ros::Duration(1.0);
      pos_marker.type = visualization_msgs::Marker::CYLINDER;
      pos_marker.pose.position.x = veh->pos_msg.pose.position.x;
      pos_marker.pose.position.y = veh->pos_msg.pose.position.y;
      pos_marker.pose.position.z = 0.0;
      pos_marker.pose.orientation.w = 1.0;
      pos_marker.color = color_list[tempIndex];
      pos_marker.color.a = 0.5;
      pos_marker.scale.x = veh->para.radius*2;
      pos_marker.scale.y = veh->para.radius*2;
      pos_marker.scale.z = 0.01;
      markerArray.markers.push_back(pos_marker);

      tempIndex++;
    }

    // === Obstacle === //
    for (int i = 0; i < obstacles.size(); i++){
      visualization_msgs::Marker obs_marker;
      obs_marker.header.frame_id = "/world";
      obs_marker.header.stamp = ros::Time::now();
      obs_marker.ns = "rvo_obs";
      obs_marker.id = i;
      obs_marker.action = visualization_msgs::Marker::ADD;
      obs_marker.lifetime = ros::Duration(1.0);
      obs_marker.type = visualization_msgs::Marker::LINE_STRIP;
      obs_marker.pose.orientation.w = 1.0;
      std_msgs::ColorRGBA color;
      color.r = 1.0;
      color.g = 0.0;
      color.b = 0.0;
      color.a = 1.0;
      obs_marker.color = color; 

      obs_marker.scale.x = 0.045;
      obs_marker.scale.y = 0.03;
      obs_marker.scale.z = 0.03;
      // Fill in the traj points
      for (int j = 0; j < obstacles[i].size(); j++){
        geometry_msgs::Point p;
        p.x = (float) obstacles[i][j].x();
        p.y = (float) obstacles[i][j].y();
        p.z = (float) 0.0;
        obs_marker.points.push_back(p);
      }
      // Add first point again
      geometry_msgs::Point p;
      p.x = (float) obstacles[i][0].x();
      p.y = (float) obstacles[i][0].y();
      p.z = (float) 0.0;
      obs_marker.points.push_back(p);


      markerArray.markers.push_back(obs_marker);
    }

    return markerArray;
  }

  bool planPath(){
    ros::Time startTime = ros::Time::now();
    RVO::RVOPathPlanner planner;
    planner.setSimParameters(goalThreshold, simTimeStep, maxSteps);

    // === Set agent and goal === //
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      planner.addAgent(vehIter->second);  
    }

    // === Add obstacles === //
    for (int i = 0; i < obstacles.size(); i++){
      planner.addObstacle(obstacles[i]);
    }

    // Generate paths using RVOPathPlanner
    bool reachGoalFlag = planner.genPath(); //This reset the traj of agents and insert new traj.

    ros::Time plan_start_time = ros::Time::now();

    // // === Save the planned trajectories === //
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      vehIter->second->plan_start_time = plan_start_time;
    }
    
    publishRvoTrajMsgs();
    return reachGoalFlag;
  }

  void publish_command_complete()
  {
    // TODO: Add timeout mechanism
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      RVOPPAgent* veh = vehIter->second;
      rvo_path_planner::CommandComplete comcom_msg;
      comcom_msg.veh_name = veh->name;
      comcom_msg.issue_time = veh->waypoints_issue_time;
      comcom_msg.header.stamp = ros::Time::now();
      comcom_msg.success = veh->hasReachedGoal();
      comcom_msg.waypoint = veh->getCurrentWayPoint();
      pub_command_complete.publish(comcom_msg);
    }
  }

  // === Callbacks === //
  void callback_veh_list(const acl_msgs::VehicleList& veh_list_msg)
  {
    // Add and remove vehicle according to the vehilce list message (published) as a part of raven_rviz
    for (int i = 0; i < veh_list_msg.vehicle_names.size(); i++){
      std::string veh_name = veh_list_msg.vehicle_names[i];
      std::string veh_type = veh_name.substr(0,2);

      // Skip if the veh_type is not handled
      if (std::find(veh_type_list.begin(), veh_type_list.end(), veh_type) == veh_type_list.end()){
        continue;
      }

      // Add vehilce if not in veh_map
      VehIter vehIter = veh_map.find(veh_name);
      if (vehIter == veh_map.end()){
        // Vehicle not in the veh_map, addVehicle
        addVehicle(veh_name);
      }
    }

    // Remove Vehicle that is not in the veh_list
    VehIter vehIter = veh_map.begin();
    while (vehIter != veh_map.end()) {
      if(std::find(veh_list_msg.vehicle_names.begin(),veh_list_msg.vehicle_names.end(),vehIter->first) == veh_list_msg.vehicle_names.end()){
        // Vehilce in veh_map not in vehilce list.
        ROS_INFO_STREAM("Removing " << vehIter->first);
        // Delete the RVOPPAgent
        delete vehIter->second;
        // Remove the key-value pair from the map
        veh_map.erase(vehIter++);
      }
      else{
        ++vehIter;
      }
    }
  }

  void callback_plan(const ros::TimerEvent& timerEvent)
  {
      planPath();
  }

  void callback_control(const ros::TimerEvent& timerEvent)
  {
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      vehIter->second->publish_quadGoal();
    }
  }

  void callback_command_complete(const ros::TimerEvent& timerEvent)
  {
    publish_command_complete();
  }
  
  void callback_markers(const ros::TimerEvent& timerEvent)
  {
    pub_traj_marker_array.publish(getTrajMarkerArray());
  }

  void callback_command(const rvo_path_planner::CommandSequence& command_seq)
  {
    std::string veh_name = command_seq.veh_name;
    VehIter vehIter = veh_map.find(veh_name);
    if (vehIter == veh_map.end()){
      // The commanded vehicle is not in the veh_map
      ROS_INFO_STREAM(node_name_str << veh_name << " not found in path_planner.");
    }
    else{
      RVOPPAgent* veh = vehIter->second;
      if (command_seq.command==rvo_path_planner::CommandSequence::MOVE){
        veh->desired_yaw = command_seq.yaw;
        veh->holdPosition = false;
        // veh->resetGoal();
        veh->setWaypoints(command_seq.waypoints,command_seq.header.stamp);
        ROS_INFO_STREAM(node_name_str << "Command " << command_seq.veh_name << " with " << command_seq.waypoints.size()<< " waypoints.");
      }
      // else if (command_seq.command==rvo_path_planner::CommandSequence::TAKEOFF_AND_MOVE){
      //   // T
      //   veh->goalTolerance_h = goalTolerance_h;
      //   veh->goalTolerance_v = goalTolerance_v;
      //   veh->desired_yaw = command_seq.yaw;
      //   veh->holdPosition = false;
      //   std::vector<geometry_msgs::Point> waypoints_vec = getAccentWaypoints(veh->pos_msg, defaultTakoffHeight);
      //   waypoints_vec.insert(waypoints_vec.end(),command_seq.waypoints.begin(),command_seq.waypoints.end());
      //   veh->setWaypoints(waypoints_vec,command_seq.header.stamp);
      //   ROS_INFO_STREAM(node_name_str << "Command " << command_seq.veh_name << " with " << command_seq.waypoints.size()<< " waypoints.");
      // }
      else if(command_seq.command==rvo_path_planner::CommandSequence::LAND){
        veh->holdPosition = true;
        // veh->resetGoal();
        geometry_msgs::Point point = veh->pos_msg.pose.position;
        point.z = -1.1;
        veh->desired_yaw = veh->getCurrentYaw();
        veh->setWaypoints(point,command_seq.header.stamp);
        ROS_INFO_STREAM(node_name_str << "Command " << command_seq.veh_name << " to " << "LAND");
      }
      else if(command_seq.command==rvo_path_planner::CommandSequence::TAKEOFF){
        // veh->desired_yaw = command_seq.yaw;
        veh->holdPosition = true;
        veh->resetGoal();
        ROS_INFO_STREAM(node_name_str << "Command " << command_seq.veh_name << " to " << "TAKEOFF");
        geometry_msgs::Point point = veh->pos_msg.pose.position;
        point.z = defaultTakoffHeight;
        veh->motorOn = true;
        veh->desired_yaw = veh->getCurrentYaw();
        veh->setWaypoints(point,command_seq.header.stamp);
      }
      else if(command_seq.command==rvo_path_planner::CommandSequence::LOW){
        veh->desired_yaw = command_seq.yaw;
        veh->holdPosition = true;
        // veh->resetGoal();
        ROS_INFO_STREAM(node_name_str << "Command " << command_seq.veh_name << " to " << "LOW");
        geometry_msgs::Point point = veh->pos_msg.pose.position;
        point.z = defaultLowHeight;
        veh->motorOn = true;
        veh->desired_yaw = veh->getCurrentYaw();
        veh->setWaypoints(point,command_seq.header.stamp);
      }
      else if(command_seq.command==rvo_path_planner::CommandSequence::KILL){
        veh->desired_yaw = command_seq.yaw;
        veh->holdPosition = true;
        ROS_INFO_STREAM(node_name_str << "Command " << command_seq.veh_name << " to " << "KILL");
        veh->motorOn = false;
      }
    }
  }

  bool callback_srv_activate(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      vehIter->second->isActivated = true;
      ROS_INFO_STREAM(vehIter->first << " Activated.");
    }
    return true;
  }

  bool callback_srv_deactivate(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      vehIter->second->isActivated = false;
      ROS_INFO_STREAM(vehIter->first << " Deactivated.");
    }
    return true;
  }

  bool callback_srv_land_all(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    // Land all the quads
    ROS_INFO_STREAM(node_name_str << "Landing all quads.");
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      rvo_path_planner::CommandSequence command_seq;
      command_seq.veh_name = vehIter->first;
      command_seq.command = rvo_path_planner::CommandSequence::LAND;
      callback_command(command_seq);
    }
    return true;
  }

  bool callback_srv_takeoff_all(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    // Activate and take off all quads to the default height
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      vehIter->second->isActivated = true;
    }
    // ROS_INFO_STREAM(node_name_str << "Activated.");
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      rvo_path_planner::CommandSequence command_seq;
      command_seq.veh_name = vehIter->first;
      command_seq.command = rvo_path_planner::CommandSequence::TAKEOFF;
      callback_command(command_seq);
    }
    return true;
  }  

  bool callback_srv_kill_all(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    // TODO: simply send KILL message
    for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
      rvo_path_planner::CommandSequence command_seq;
      command_seq.veh_name = vehIter->first;
      command_seq.command = rvo_path_planner::CommandSequence::KILL;
      callback_command(command_seq);
    }
    // // Kill all motors right away and deactivate
    // for (VehIter vehIter = veh_map.begin(); vehIter != veh_map.end(); vehIter++){
    //   RVOPPAgent* veh = vehIter->second;
    //   // === Set current pos as waypoints === //
    //   veh->setWaypoints(veh->pos_msg.pose.position, ros::Time::now());
    //   veh->motorOn = false;
    // }
    return true;
  }
};

void setDefaultParameters()
{
  // Set default parameters
  if (!ros::param::has("~goalThreshold")) { ros::param::set("~goalThreshold",0.1);}
  if (!ros::param::has("~simTimeStep")) { ros::param::set("~simTimeStep",0.01);}
  if (!ros::param::has("~maxSteps")) { ros::param::set("~maxSteps",300);}
  if (!ros::param::has("~defaultTakoffHeight")) { ros::param::set("~defaultTakoffHeight",1.0);}
  if (!ros::param::has("~defaultLandHeight")) { ros::param::set("~defaultLandHeight",0.0);}
  if (!ros::param::has("~defaultLowHeight")) { ros::param::set("~defaultLowHeight",0.5);}
  if (!ros::param::has("~room_type")) { ros::param::set("~room_type",0);}
  if (!ros::param::has("~command_interval")) { ros::param::set("~command_interval",0.01);}
  if (!ros::param::has("~planning_interval")) { ros::param::set("~planning_interval",0.01);}
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "path_planner");
  RVOPPNode path_node;
  setDefaultParameters();

  double command_interval;
  ros::param::getCached("~command_interval",command_interval);
  double planning_interval;
  ros::param::getCached("~planning_interval",planning_interval);

  ros::Timer timer_plan = path_node.nh.createTimer(ros::Duration(planning_interval), &RVOPPNode::callback_plan, &path_node);
  ros::Timer timer_contorl = path_node.nh.createTimer(ros::Duration(command_interval), &RVOPPNode::callback_control, &path_node);
  ros::Timer timer_command_complete = path_node.nh.createTimer(ros::Duration(0.1), &RVOPPNode::callback_command_complete, &path_node);
  ros::Timer timer_markers = path_node.nh.createTimer(ros::Duration(1.0/60.0), &RVOPPNode::callback_markers, &path_node);

  ros::spin();

  // TODO: Figure out how asyncspinner work.
  // ros::AsyncSpinner spinner(4);
  // spinner.start();
  // ros::waitForShutdown();
  return 0;
}