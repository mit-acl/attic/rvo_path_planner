#!/usr/bin/env python
# license removed for brevity
import rospy
# Include the ROS msg to be send
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseStamped
from acl_msgs.msg import QuadGoal

# from rvo_path_planner.msg import CommandSequence
# from rvo_path_planner.msg import CommandComplete
from rvo_path_planner.msg import RvoTraj, RvoRoom
from acl_msgs.msg import VehicleList
from visualization_msgs.msg import MarkerArray,Marker
from std_msgs.msg import ColorRGBA

import copy

# import numpy as np
# import random
import time
import numpy as np

#TODO should figure out how to do this without copying the code.
import udputil
import projutil

color_list = [
    ColorRGBA(1.0,0.0,0.0,1.0),
    ColorRGBA(0.0,1.0,0.0,1.0),
    ColorRGBA(0.0,0.0,1.0,1.0),
    ColorRGBA(1.0,0.0,1.0,1.0),
    ColorRGBA(1.0,1.0,0.0,1.0),
    ColorRGBA(0.0,1.0,1.0,1.0),
    ColorRGBA(0.5,0.0,0.0,1.0),
    ColorRGBA(0.0,0.5,0.0,1.0),
    ColorRGBA(0.0,0.0,0.5,1.0)]

class QuadObj(object):
    def __init__(self,name,host,port,index):
        self.name = name
        self.index = index
        self.rvo_traj_msg = RvoTraj()
        self.sub_rvo_traj = rospy.Subscriber('%s/rvo_traj'%self.name, RvoTraj, self.cb_rvo_traj)
        self.udp_sender = udputil.UdpSender(host,port)
        rospy.loginfo("[Visualizer] %s send to %s:%s"%(name,host,port))

    def cb_rvo_traj(self,rvo_traj_msg):
        self.rvo_traj_msg = copy.deepcopy(rvo_traj_msg)
        # print "%s:%s"%(self.name,self.rvo_traj_msg.pos_current)

class RvoVisualizer(object):
    def __init__(self):
        self.host = rospy.get_param("~host")
        self.cali_file_path = rospy.get_param("~cali_file_path")
        self.converter = projutil.PosConverter(self.cali_file_path+'calibration_data.pickle',
            self.cali_file_path+'calibration_data_side.pickle')

        # self.sub_vehlist = rospy.Subscriber("current_vehicles", VehicleList , self.cb_veh_list)
        self.quad_dict = dict()
        self.veh_names = rospy.get_param("~vehicle_list")
        self.port_list = rospy.get_param("~port_list")
        for index,name in enumerate(self.veh_names):
            self.quad_dict[name] = QuadObj(name,self.host,self.port_list[index],index)
            # self.add_vehilce(name,self.port_list[index],index)

        self.sub_rvo_room = rospy.Subscriber("rvo_path_planner/rvo_room", RvoRoom, self.cb_room)
        self.rvo_room_msg = RvoRoom()
        self.pub_marker = rospy.Publisher('/RVO_Visuals',MarkerArray,queue_size=1)

    # def cb_veh_list(self,veh_list_msg):
    #     for veh_name in veh_list_msg.vehicle_names:
    #         if veh_name not in self.quad_dict.keys():
    #             self.add_vehilce(veh_name)

    # def add_vehilce(self,veh_name,port,index):

    def cb_room(self,rvo_room_msg):
        self.rvo_room_msg = copy.deepcopy(rvo_room_msg)

    def cb_pub_markers(self,event):
        self.publishMarkerArray()

    def cb_send_udp(self,event):
        self.send_udp()

    def send_udp(self):
        for veh_name, quad in self.quad_dict.iteritems():
            rvo_traj_msg = quad.rvo_traj_msg
            # Position
            pos_data = np.array([rvo_traj_msg.pos_current.x,rvo_traj_msg.pos_current.y])
            pos_data = self.converter.toProjPos(pos_data)
            pos_data = np.append(pos_data,rvo_traj_msg.pos_current.z)
            quad.udp_sender.send('rvo_pos',pos_data,time.time())

            # Trajectory
            traj_data = np.array([[point.x, point.y] for point in rvo_traj_msg.pos_traj])
            if len(traj_data) > 0:
                traj_data = self.converter.toProjPos(np.array(traj_data))
                quad.udp_sender.send('rvo_path',traj_data,time.time())
                

    # def publishMarkerArray(self):
    #     markerArray = MarkerArray()
    #     for veh_name, quad in self.quad_dict.iteritems():
    #         rvo_traj_msg = copy.deepcopy(quad.rvo_traj_msg)
    #         # Trajectory
    #         traj_marker = Marker()
    #         traj_marker.header.frame_id = "/world"
    #         traj_marker.header.stamp = rospy.Time.now()
    #         traj_marker.ns = "rvo_path"
    #         traj_marker.id = quad.index
    #         traj_marker.action = Marker.ADD
    #         traj_marker.lifetime = rospy.Duration.from_sec(1.0)
    #         traj_marker.type = Marker.LINE_STRIP
    #         traj_marker.pose.orientation.w = 1.0
    #         traj_marker.color = color_list[quad.index] #TODO set color_list
    #         traj_marker.scale.x = 0.045
    #         traj_marker.scale.y = 0.03
    #         traj_marker.scale.z = 0.03
    #         traj_marker.points = copy.deepcopy(rvo_traj_msg.pos_traj)
    #         markerArray.markers.append(traj_marker)
            
    #         # RVO disks
    #         pos_marker = Marker()
    #         pos_marker.header.frame_id = "/world"
    #         pos_marker.header.stamp = rospy.Time.now()
    #         pos_marker.ns = "rvo_pos"
    #         pos_marker.id = quad.index
    #         pos_marker.action = Marker.ADD
    #         pos_marker.lifetime = rospy.Duration.from_sec(1.0)
    #         pos_marker.type = Marker.CYLINDER
    #         pos_marker.pose.position = copy.deepcopy(rvo_traj_msg.pos_current)
    #         pos_marker.pose.position.z = 0.0
    #         pos_marker.pose.orientation.w = 1.0
    #         pos_marker.color = color_list[quad.index] #TODO set color_list

    #         pos_marker.scale.x = rvo_traj_msg.radius*2
    #         pos_marker.scale.y = rvo_traj_msg.radius*2
    #         pos_marker.scale.z = 0.01
    #         markerArray.markers.append(pos_marker)

    #     # Room
    #     room_marker = Marker()
    #     room_marker.header.frame_id = "/world"
    #     room_marker.header.stamp = rospy.Time.now()
    #     room_marker.ns = "rvo_room"
    #     room_marker.id = 0
    #     room_marker.action = Marker.ADD
    #     room_marker.lifetime = rospy.Duration.from_sec(0.0)
    #     room_marker.type = Marker.LINE_STRIP
    #     room_marker.pose.orientation.w = 1.0
    #     room_marker.color = ColorRGBA(1.0,0.0,0.0,1.0)
    #     room_marker.scale.x = 0.045
    #     room_marker.scale.y = 0.03
    #     room_marker.scale.z = 0.03
    #     room_marker.points = self.rvo_room_msg.room
    #     if len(room_marker.points) > 0:
    #         room_marker.points.append(room_marker.points[0])
    #     markerArray.markers.append(room_marker)

    #     # Pillar
    #     pillar_marker = Marker()
    #     pillar_marker.header.frame_id = "/world"
    #     pillar_marker.header.stamp = rospy.Time.now()
    #     pillar_marker.ns = "rvo_room"
    #     pillar_marker.id = 1
    #     pillar_marker.action = Marker.ADD
    #     pillar_marker.lifetime = rospy.Duration.from_sec(0.0)
    #     pillar_marker.type = Marker.LINE_STRIP
    #     pillar_marker.pose.orientation.w = 1.0
    #     pillar_marker.color = ColorRGBA(1.0,0.0,0.0,1.0)
    #     pillar_marker.scale.x = 0.045
    #     pillar_marker.scale.y = 0.03
    #     pillar_marker.scale.z = 0.03
    #     pillar_marker.points = self.rvo_room_msg.pillar
    #     if len(pillar_marker.points) > 0:
    #         pillar_marker.points.append(pillar_marker.points[0])
    #     markerArray.markers.append(pillar_marker)
    #     self.pub_marker.publish(markerArray)

if __name__ == '__main__':
    rospy.init_node('rvo_visualizer',anonymous=False)
    visualizer = RvoVisualizer()
    # pub_marker_timer = rospy.Timer(rospy.Duration.from_sec(1.0/30.0),visualizer.cb_pub_markers)
    pub_udp_timer = rospy.Timer(rospy.Duration.from_sec(1.0/60.0),visualizer.cb_send_udp)
    rospy.spin()
