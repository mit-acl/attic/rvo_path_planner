'''
Created on Nov 21, 2014

@author: brettlopez
@author: Shih-Yuan Liu
'''

import socket as sk
import pickle
import time
import select

from collections import namedtuple
CommData = namedtuple("CommData", "id data timestamp")

class UdpSender(object):
    def __init__(self,host,port):
        # Initialize the comm with host and port
        self.UDPSock = sk.socket(sk.AF_INET,sk.SOCK_DGRAM)
        self.host = host
        self.port = port
        print '[UdpSender] initialized with host %s port %s' %(self.host,self.port)

    def __del__(self):
        self.UDPSock.close()

    def send(self,msg_id,msg_data,msg_timestamp=None):      
        # Pickle data and deliver through UDP

        # Set timestamp to deliver time if not provided
        if(msg_timestamp==None):
            msg_timestamp = time.time()

        # Pickle data using the nameedtuple CommData
        pick_data = pickle.dumps(CommData(msg_id,msg_data,msg_timestamp))

        # Deliver through UDP to the destination
        if(self.UDPSock.sendto(pick_data,(self.host,self.port))):
            pass
            # print "[UdpSender] Sending message to %s:%s. Size %s. Size %s" %(self.host,self.port,len(msg_data),len(pick_data))

class UdpReceiver(object):
    def __init__(self,host,port,buf=4096):
        # Initialize the comm with host and port
        self.UDPSock = sk.socket(sk.AF_INET,sk.SOCK_DGRAM)
        self.host = host
        self.port = port
        self.buf = buf

        self._data_list = list()
        self._data_dict = dict()

        self.UDPSock.bind((self.host,self.port))
        print '[UdpReceiver] Binding: %s:%s' %(self.host,self.port)  
        
    def __del__(self):
        self.UDPSock.close()
        print "[UdpReceiver] Closing %s:%s" %(self.host,self.port)

    def _ready_to_read(self):
        # Return True if the socket is ready to read (still has msg in queue), False if not.
        socketlist = [self.UDPSock]
        readReadyList, writeReadyList, errorList = select.select(socketlist,[],[],0.0)
        if len(readReadyList)==0:
            # socket not ready to read, return None
            return False
        else:
            return True

    def _read_one_from_queue(self):
        # Receive messages and return the unpickled data
        up_data = self.UDPSock.recv(self.buf)
        # print up_data
        return pickle.loads(up_data)
        # print "[Protocol.comm] Received message through host %s port %s" %(self.host,self.port)

    def _read_all_from_queue(self):
        # Return a list of CommData in the socket (can be empty list)
        msgList = list()
        while self._ready_to_read():
            msgList.append(self._read_one_from_queue())
        return msgList

    def update(self):
        data_list = self._read_all_from_queue()
        if len(data_list) > 0:
            self._data_list = data_list
            self._data_dict = dict()
            # print '%s [UdpReceiver]: received %s commData.' %(time.time(),len(self._data_list))
            # Add data to dict according to id
            for commData in self._data_list:
                # Start an empty list for new id
                if commData.id not in self._data_dict.keys():
                    self._data_dict[commData.id] = list()

                # Add data to dict
                self._data_dict[commData.id].append(commData)
            return True
        else:
            return False

    def get_latest_data(self):
        # Return a dict indexed by id and only containing the latest data
        latest_dict = dict()
        for key in self._data_dict.keys():
            latest_data = max(self._data_dict[key], key = lambda x: x.timestamp)
            latest_dict[key] = latest_data
        return latest_dict

    # Return the data received via UDP, not only the latest data like get_latest_data
    def get_new_data(self):
        return self._data_dict
    
    def get_all_data(self):
        return self._data_dict